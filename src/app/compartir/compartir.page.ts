import { Component, OnInit } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { Share } from "@capacitor/share";
import { IonButtons, IonHeader, IonMenuButton, IonTitle, IonToolbar, IonContent, IonText, IonTextarea, IonButton, IonLabel } from "@ionic/angular/standalone";

@Component({
    templateUrl: './compartir.page.html',
    styleUrl: './compartir.page.scss',
    standalone: true,
    imports: [IonLabel, IonButton, IonTextarea, IonText, IonContent, IonHeader, IonToolbar, IonButtons, IonMenuButton, IonTitle, FormsModule],
})
export class CompartirPage implements OnInit {
    contenidoParaCompartir: string = '';

    ngOnInit(): void {
        console.log('onInit');
    }

    compartirContenido() {
        Share.share({
            title:'Contenido desde App Ionic',
            text: this.contenidoParaCompartir
        })
            .then(result => console.log('Se compartió', result.activityType));
    }
        
}