import { Component, OnInit } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { Clipboard } from "@capacitor/clipboard";
import { IonButtons, IonHeader, IonMenuButton, IonTitle, IonToolbar, IonContent, IonText, IonTextarea, IonButton, IonLabel } from "@ionic/angular/standalone";

@Component({
    templateUrl: './clipboard.page.html',
    styleUrl: './clipboard.page.scss',
    standalone: true,
    imports: [IonLabel, IonButton, IonTextarea, IonText, IonContent, IonHeader, IonToolbar, IonButtons, IonMenuButton, IonTitle, FormsModule],
})
export class ClipboardPage implements OnInit {

    contenidoParaEscribir: string = '';
    contenidoPegado: any;
    tipoContenidoPegado: string = '';

    ngOnInit(): void {
        console.log('onInit');
    }

    copiarArea() {
        Clipboard.write({string:this.contenidoParaEscribir})
            .then(() => {console.log('Se escribió')})
            .catch(err => console.log('ERROR', err));
    }

    leerPortapapeles() {
        Clipboard.read()
            .then(res => {
                this.tipoContenidoPegado = res.type;
                this.contenidoPegado = res.value;
            })
    }
        
        
}