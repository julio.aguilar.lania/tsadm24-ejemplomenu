import { Routes } from '@angular/router';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'folder/inbox',
    pathMatch: 'full',
  },
  {
    path: 'folder/:id',
    loadComponent: () =>
      import('./folder/folder.page').then((m) => m.FolderPage),
  },
  {
    path: 'clipboard',
    loadComponent: () => import('./clipboard/clipboard.page').then((m) => m.ClipboardPage)
  },
  {
    path: 'compartir',
    loadComponent: () => import('./compartir/compartir.page').then((m) => m.CompartirPage)
  },
  {
    path: 'camara',
    loadComponent: () => import('./camara/camara.page').then((m) => m.CamaraPage)
  }
];
