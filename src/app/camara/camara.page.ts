import { Component, OnInit } from "@angular/core";
import { Camera, CameraResultType, CameraSource, Photo } from "@capacitor/camera";
import { IonHeader,IonToolbar,IonButtons,IonMenuButton,IonTitle,IonContent, IonButton, IonLabel,IonList,IonItem } from "@ionic/angular/standalone";

@Component({
    templateUrl:'./camara.page.html',
    styleUrl:'./camara.page.scss',
    imports:[IonHeader,IonToolbar,IonButtons,IonMenuButton,IonTitle,IonContent,IonButton,IonLabel,IonList,IonItem],
    standalone:true
})
export class CamaraPage implements OnInit {
    informacionFoto?: Photo;
    ngOnInit(): void {
        console.log('onInit');
    }

    usarCamra(): void {
        console.log('intentando usar la camara')
        Camera.checkPermissions().then(ps => {
            if (ps.photos === 'granted') {
                this.tomarFotos();
            }
            else {
                Camera.requestPermissions({permissions:['camera','photos']})
                    .then(
                        ps => {
                            if (ps.photos === 'granted') {
                                this.tomarFotos();
                            }
                        }
                    )
            }
        });
    }

    tomarFotos(): void {
        console.log('tomando foto');
        Camera.getPhoto({
            source: CameraSource.Camera,
            resultType: CameraResultType.Uri
        })
            .then(photo => {
                this.informacionFoto = photo;
            });
    }

    ionViewWillEnter() {
        this.informacionFoto = undefined;
    }

}