import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'mx.mrysi.EjemploMenu.2',
  appName: 'EjemploMenu',
  webDir: 'www',
  server: {
    androidScheme: 'https'
  }
};

export default config;
